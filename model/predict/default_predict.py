# -*- coding:utf-8 -*-


def predict(self, x):
    """
    默认predict
    :return:
    """
    __y_pred = self.sess.run(self.y_pred, feed_dict={self.xs: x})
    return __y_pred
