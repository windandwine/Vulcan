# -*- coding:utf-8 -*-

from __future__ import division
import tensorflow as tf
from element.dnn_module import fc_layer

"""
单层DNN示例
如果新建一个模型有三种方法，
1.继承(推荐)
2.直接声明
"""


def net(self):
    with tf.name_scope('fc_1'):
        layer1 = fc_layer(self.xs, 10, activation_func=tf.nn.softmax)  # 隐藏层
    with tf.name_scope('fc_2'):
        self.y_pred = fc_layer(layer1, 10, activation_func=tf.nn.softmax)  # 隐藏层
