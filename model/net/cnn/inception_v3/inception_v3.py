# -*- coding:utf-8 -*-

import tensorflow as tf
import tensorflow.contrib.slim as slim
import os


def inception_v3(net):
    """
    旧方法，未融合
    :param net:
    :return:
    """
    with slim.arg_scope(
            [slim.conv2d, slim.max_pool2d, slim.avg_pool2d],
            stride=1,
            padding='SAME'):
        # 为一个inception模块一个同一命名空间
        with tf.variable_scope('Mixed_7c'):
            # 为一个模块的路径声明一个命名空间，Inception的第二1条路径
            with tf.variable_scope('Branch_0'):
                branch_0 = slim.conv2d(net, 320, [1, 1], scope='Conv2d_0a_1x1')

            # Inception的第2条路径
            with tf.variable_scope('Branch_1'):
                branch_1 = slim.conv2d(net, 384, [1, 1], scope='Conv2d_0a_1x1')
                # tf.concat可以拼接多个矩阵
                branch_1 = tf.concat(
                    3,
                    [slim.conv2d(branch_1, 384, [1, 3], scope='Conv2d_0b_1x3'),
                     slim.conv2d(branch_1, 384, [3, 1], scope='Conv2d_0c_3x1')]
                )
            # Inception的第3条路径
            with tf.variable_scope('Branch_2'):
                branch_2 = slim.conv2d(net, 448, [1, 1], scope='Conv2d_0a_1x1')
                branch_2 = slim.conv2d(branch_2, 384, [1, 1], scope='Conv2d_0a_1x1')
                branch_2 = tf.concat(
                    3,
                    [slim.conv2d(branch_2, 384, [1, 3], scope='Conv2d_0b_1x3'),
                     slim.conv2d(branch_2, 384, [3, 1], scope='Conv2d_0c_3x1')]
                )
            # Inception的第4条路径
            with tf.variable_scope('Branch_3'):
                branch_3 = slim.avg_pool2d(net, [3, 3], scope='AvgPool_0a_3x3')
                branch_3 = slim.conv2d(branch_3, 192, [1, 1], scope='Conv2d_0b_1x1')

            net = tf.concat([branch_0, branch_1, branch_2, branch_3], 3)

