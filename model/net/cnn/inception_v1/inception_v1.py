# -*- coding:utf-8 -*-

import tensorflow as tf
import tensorflow.contrib.slim as slim
# import 4_tensorflow.contrib.slim.nets as nets
import os


def inception_simple(net):
    """
    基本inception（旧方法未融合） rest
    :return:
    """
    vgg = tf.contrib.slim.nets.vgg
    with slim.arg_scope(
            [slim.conv2d, slim.max_pool2d, slim.avg_pool2d],
            stride=1,
            padding='SAME'):
        with tf.variable_scope('inception_block'):
            with tf.variable_scope('Branch_0'):
                branch_0 = slim.conv2d(net, 320, [1, 1], scope='Conv2d_0a_1x1')
            with tf.variable_scope('Branch_1'):
                branch_1 = slim.conv2d(net, 320, [3, 3], scope='Conv2d_0a_3x3')
            with tf.variable_scope('Branch_2'):
                branch_2 = slim.conv2d(net, 320, [5, 5], scope='Conv2d_0a_5x5')
            with tf.variable_scope('Branch_3'):
                branch_3 = slim.max_pool2d(net, 320, [3, 3], scope='MaxPool_0a_3x3')
            net = tf.concat([branch_0, branch_1, branch_2, branch_3], 3)
    return net


def inception_v1(net):
    """
    Inception v1
    :return:
    """
    with slim.arg_scope(
            [slim.conv2d, slim.max_pool2d, slim.avg_pool2d],
            stride=1,
            padding='SAME'):
        with tf.variable_scope('inception_v1_block'):
            with tf.variable_scope('Branch_0'):
                branch_0 = slim.conv2d(net, 320, [1, 1], scope='Conv2d_0a_1x1')
            with tf.variable_scope('Branch_1'):
                branch_1 = slim.conv2d(net, 320, [3, 3], scope='Conv2d_0a_3x3')
            with tf.variable_scope('Branch_2'):
                branch_2 = slim.conv2d(net, 320, [5, 5], scope='Conv2d_0a_5x5')
            with tf.variable_scope('Branch_3'):
                branch_3 = slim.max_pool2d(net, 320, [3, 3], scope='MaxPool_0a_3x3')
            net = tf.concat([branch_0, branch_1, branch_2, branch_3], 3)
    return net
