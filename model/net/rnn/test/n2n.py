# -*- coding:utf-8 -*-

import numpy as np
import tensorflow as tf
from element.rnn_module import rnn_cell_custom


def net():
    """
    RNN
    :return:
    """

    xs = tf.placeholder(tf.float32, [2, 1], name='xs')
    ys = tf.placeholder(tf.float32, [2, 1], name='ys')
    out = rnn_cell_custom(xs, 10, 5, 2)

    with tf.Session() as sess:
        initer = tf.global_variables_initializer()
        initer.run()
        __out = sess.run(out, feed_dict={xs: x})
        print(__out)

