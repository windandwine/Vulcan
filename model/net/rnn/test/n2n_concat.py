# -*- coding:utf-8 -*-

import numpy as np
import tensorflow as tf
from element.rnn_module import rnn_cell_concat


def net():
    """
    RNN
    :return:
    """
    x = np.array([[1], [2]], dtype=np.float32)
    xs = tf.placeholder(tf.float32, [2, 1], name='xs')
    out = rnn_cell_concat(xs, 10, 5, 2)

    with tf.Session() as sess:
        initer = tf.global_variables_initializer()
        initer.run()
        __out = sess.run(out, feed_dict={xs: x})
        print(__out)
