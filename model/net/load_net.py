# -*- coding:utf-8 -*-

import importlib
from config.glob.global_pool import global_pool


def set_net_in_model(model):
    """
    动态加载net模块，置入Model
    :return:
    """
    module = importlib.import_module(global_pool.config.net.module)
    setattr(model, 'net', module.net)
    model.net(model)
    return model
