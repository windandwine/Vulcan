# -*- coding:utf-8 -*-

import os
import pickle
import numpy as np
import tensorflow as tf
import config.glob.config as global_cfg
from utils.log_utils import log_path, ClampLog
from tensorflow.contrib.tensorboard.plugins import projector
from config.glob.global_pool import global_pool
from utils.common_utils import Singleton
from utils.tb_utils import run_tensorboard

"""
针对Chinese Word Vector词嵌入(https://github.com/Embedding/Chinese-Word-Vectors)的解析
"""


class Embedding(Singleton):
    """
    将模型保存为dict(key,300d ndarry)格式
    """
    def __init__(self, model_path):
        """
        :param model_path: 已处理过的模型
        """
        self.char_idx = None
        self.idx_char = None
        self.embed = None
        self.embed_var = None
        self.vocab_size = 0
        self.file_path = model_path  # 原始数据
        pre_dir = os.path.split(model_path)[0]  # 上一级菜单
        self.model_path = os.path.join(pre_dir, 'embed')  # 已处理过的模型
        self.visual_dir = os.path.join(pre_dir, 'visual')
        self.build_embed()
        if global_pool.config.is_train:
            self.graph = tf.Graph()
            self.sess = tf.Session(graph=self.graph)
            self.save_visual_metadata()
            # 打开tensorboard
            run_tensorboard(self.visual_dir, global_pool.config.tb_port)
            global_pool.config.tb_port = global_pool.config.tb_port + 1  # 端口+1
            self.sess.close()

    def build_embed(self):
        """
        构建词嵌入
        :return:
        """
        if os.path.exists(self.model_path):  # 如果有模, 直接加载模型
            with open(self.model_path, 'rb') as f:
                load_data = pickle.load(f)
                self.char_idx = load_data[0]
                self.idx_char = load_data[1]
                self.embed = load_data[2]
                self.vocab_size = load_data[3]
        else:  # 否则从文件构建
            self.char_idx = dict()
            self.idx_char = list()
            embed = list()
            # self.char_idx['Unknown'] = 0
            # self.idx_char.append('Unknown')
            embed.append(np.zeros(300, dtype=float))
            with open(self.file_path, encoding='utf-8') as f:
                i = 0
                for line in f.readlines():
                    one_char = line.split()
                    if len(one_char) == 301:
                        self.char_idx[one_char[0]] = i
                        self.idx_char.append(one_char[0])
                        embed.append(np.array(one_char[1:]))
                        i += 1
            self.embed = np.array(embed, dtype=np.float32)
            self.save_to_file()

    def word_to_idx(self, word):
        """
        文字转idx
        :param word:
        :return:
        """
        return self.char_idx.get(word, 0)

    def idx_to_word(self, idx):
        """
        文字转idx
        :param word:
        :return:
        """
        return self.idx_char[idx]

    def word_to_embed(self, word):
        """
        文字转embedding
        :param word:
        :return:
        """
        return self.embed[self.word_to_idx(word)]

    def text_to_idx_arr(self, text):
        """
        句子转idx 数组
        :param text:
        :return:
        """
        arr = []
        for word in text:
            arr.append(self.word_to_idx(word))
        return np.array(arr)

    def text_to_embed_arr(self, text):
        """
        句子转embedding 数组
        :param text:
        :return:
        """
        arr = []
        for word in text:
            arr.append(self.word_to_embed(word))
        return np.array(arr)

    def idx_arr_to_text(self, arr):
        text = ''
        for idx in arr:
            text = text + self.idx_to_word(idx)
        return text

    def set_vocab_size(self, text):
        self.vocab_size = len(set(text))

    def save_to_file(self):
        """
        保存为模型文件
        :return:
        """
        with open(self.model_path, 'wb') as f:
            pickle.dump([self.char_idx, self.idx_char, self.embed, self.vocab_size], f)

    def save_visual_metadata(self):
        """
        保存词嵌入所需的可视化文件
        :return:
        """
        with ClampLog('embedding visual'):
            # 新建visual文件夹
            if not os.path.exists(self.visual_dir):
                os.makedirs(self.visual_dir)
            # 新建一个metadata
            metadata_path = os.path.join(self.visual_dir, 'metadata.tsv')
            if not os.path.exists(metadata_path):
                with open(metadata_path, 'w', encoding='utf-8') as f:
                    f.write("Index\tLabel\n")
                    for char, idx in self.char_idx.items():
                        f.write("{}\t{}\n".format(idx, char))
            # model与日志
            if not os.path.exists(os.path.join(self.visual_dir, "checkpoint")):
                with self.graph.as_default():
                    embed_var = tf.Variable(self.embed, name='embeddingvar')
                    summary_writer = tf.summary.FileWriter(self.visual_dir)
                    # projector部分
                    config = projector.ProjectorConfig()
                    embedding = config.embeddings.add()
                    embedding.tensor_name = embed_var.name
                    embedding.metadata_path = os.path.join(self.visual_dir, "metadata.tsv")
                    projector.visualize_embeddings(summary_writer, config)
                    # 保存模型
                    self.sess.run(tf.global_variables_initializer())
                    saver = tf.train.Saver()
                    saver.save(self.sess, os.path.join(self.visual_dir, "model"))


def build_embed():
    """
    构建词汇表
    :return:
    """
    embed_path = os.path.join(global_cfg.EMBEDDING_ROOT, global_pool.config.embed.data_path)
    log_path(embed_path, 'ori embedding path')
    # 有模型则直接加载, 否则重新生成
    embedding = Embedding(embed_path)
    return embedding
