# -*- coding:utf-8 -*-

import importlib
from utils.log_utils import ClampLog
from config.glob.global_pool import global_pool
from utils.log_utils import log_debug


"""
embedding构建/加载
"""


def build_embed():
    """
    建立embedding
    :return:
    """
    if global_pool.config.embed:  # 是否使用预处理
        with ClampLog('building embed'):
            # 加载embedding
            embed_module = importlib.import_module(global_pool.config.embed.module)
            global_pool.embedding = embed_module.build_embed()
    else:
        log_debug('no embed config, do not load embed func', fore='dy')
