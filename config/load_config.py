# -*- coding:utf-8 -*-

import os
import json
from utils.io_utils import get_yml, convert_abspath
from utils.data_utils import dict_merge
from utils.io_utils import generate_model_dir
from utils.log_utils import ClampLog, log_debug, log_path
from utils.data_utils import DictObj
from config.glob.global_pool import global_pool

"""
加载配置
"""


def validate_config(config):
    """
    校验配置
    :param config:
    :return:
    """
    with ClampLog('validating config'):
        # net必须有
        assert config.net and config.net.module, \
            '"net" and "net.module" have to exist in config'
        # xs ys的四项必须有
        assert config.xs_shape and config.ys_shape, \
            '"xs_shape" and "ys_shape" have to exist in config'
        assert config.xs_dtype and config.ys_dtype, \
            'xs_dtype" and "ys_dtype" have to exist in config'
        # batch_size必须有
        assert config.batch_size, \
            '"batch_size" has to exist in config'
        # embed可以有
        if config.embed:
            assert config.embed.module, \
                'If "embed" exists in config, "embed.module" has to exist'
        # 训练需要配置校验
        if config.is_train:
            assert config.reader and config.reader.dataset and config.reader.module, \
                'If train, "reader", "reader.dataset" and "reader.module" have to exist in config'
            assert config.epoch, \
                'If train, "epoch" has to exist in config'
            assert config.save, \
                'If train, "save" has to exist in config'
            assert config.loss and config.loss.reduction, \
                'If train, "loss" and "loss.reduction" have to exist in config'
            if config.loss.self_losser:
                assert config.loss.losser, \
                    'If train, "loss.self_losser"=true, "loss.losser" have to exist in config'
            else:
                assert config.loss.name, \
                    'If train, "loss.self_losser"=false, "loss.name" have to exist in config'
            assert config.optimizer and config.optimizer.name, \
                'If train, "optimizerr" and "optimizer.name" have to exist in config'
            assert config.learning_rate and config.learning_rate.name and config.learning_rate.value, \
                'If train, "learning_rate", "learning_rate.name" and "learning_rate.value" have to exist in config'
            if config.learning_rate.name == 'exp_decay':
                assert config.learning_rate.decay_rate and config.learning_rate.decay_step, \
                    'If train, "loss.self_losser"=true, "loss.losser" have to exist in config'
        else:
            assert config.predict_mod, \
                'If predict, "predict_mod" has to exist in config'
            assert config.load_model_dir, \
                'If predict, "load_model_dir" has to exist in config'
        return True


def set_model_dir(config):
    """
    生成日志和模型保存路径
    :param config:
    :return:
    """
    folder_dir = generate_model_dir(config)
    # 日志路径
    config.log_dir = folder_dir + 'log/'
    log_path(config.log_dir, 'save tensorboard to')
    # 模型保存path
    if config.save.is_save:
        model_dir = folder_dir + 'model/'
        log_path(model_dir, 'save model to')
        config.model_path = model_dir + 'model'
    return config


def load_config(path, is_train):
    # 加载配置
    with ClampLog('loading config'):
        # 自定义的配置
        user_cfg = get_yml(path)
        # 默认配置
        default_dir = convert_abspath('config/core/default/')
        default_cfg = get_yml(os.path.join(default_dir, 'default_train.yml')) \
            if is_train else get_yml(os.path.join(default_dir, 'default_predict.yml'))
        # merge
        cfg = dict_merge(user_cfg, default_cfg)
        # 打印日志
        log_debug(json.dumps(cfg, indent=4, separators=(',', ':')), fore='p')
        config_cls = DictObj(cfg)
        config_cls.is_train = is_train  # 加入is_train字段
        if validate_config(config_cls):  # 校验通过
            if is_train:
                config_cls = set_model_dir(config_cls)  # 设置模型与日志路径
            global_pool.config = config_cls  # 放入global_pool
