此文件夹是核心配置，用来控制训练预测流程、训练预测参数

**在train和predict文件夹中存放自定义配置**，**在default文件夹中存放默认配置**，会自动用自定义配置merge默认配置，也就是说，**如果默认配置中已经存在，且key-value都相同，在自定义配置中可以无需再次定义**。

# 一、常规配置

```yaml
#--- 常规配置-----
dataset_path: 'poetry'  # 数据集地址(会自动拼接global中的前缀地址)
module: 'model.net.dnn.single_layer.single_layer'
xs_shape: [26]  # xs shape
ys_shape: [26]  # ys shape
```

# 二、文件饥饿日志配置

```yaml
#--- 文件配置-----
# 保存配置
save:
  is_save: false  # 是否保存模型
  model_dir: "data/model/rnn/drnn/"  # 模型保存地址
# 日志选项
log:
  board_dir: "data/board/rnn/drnn/"  # board event日志地址
  log_dir: ""  # 日志地址（未实现）
```

save.model_dir 默认路径在 data/model 下，log.board_dir  默认路径在 data/board下，log.log_dir  默认路径在 data/log下。log。如果配置这三项，这按照配置路径存储。

# 三、加载配置

```yaml
#--  加载配置-----
load_dataset_mod: 'dataload.load_text'  # 加载原始数据集文件方法
```

# 四、预处理配置

```yaml
#--- 预处理配置-----
preprocess:
  use: true  # 是否使用预处理
  module: 'dataset.preprocessor.text_preprocessor'  # 自定义预处理模块
  pre_data_path: 'converter.pkl'   # 保存预处理后数据集路径（如已有,则加载）
```

# 五、dataset配置

```yaml
#--- dataset配置-----
ds:
  use_handle: true   # 是否在dataset中使用自定义handle
  handle: 'dataset.parser.mnist_parser'  # 自定义handle
```

# 六、net配置

```yaml
#--- net配置-----
net:
  module: 'model.net.rnn.drnn.drnn'
  ...  # 需要在net中加载的配置
```

# 六、train配置

```yaml
#--- train配置-----
epoch: 50  # epoch
batch_size: 10  # batch大小
# 损失函数
loss:
  self_losser: true  # 是否自定义损失函数
  losser: 'model.loss.loss.text_loss'  # 损失函数
  reduction: 'sum'
# 优化方法
optimizer:
  name: 'adam'
  use_clip: true  # 是否使用clipping gradients
# 学习率
learning_rate:
  name: 'exp_decay'  # 学习率方法(fixed, exp_decay)
  value: !!float 1e-3  # 学习率
  decay_rate: 0.99  # 衰减率(exp_decay才需要)
  decay_step: 100  # 衰减步长(exp_decay才需要)
# 验证方法配置
accuracy: 'default'
```