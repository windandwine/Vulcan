# -*- coding:utf-8 -*-

from dto.global_pool_dto import GlobalPoolDto

"""
全局变量池，存放各个模块的全局变量
"""

global_pool = GlobalPoolDto()

