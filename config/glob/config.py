# -*- coding:utf-8 -*-

import os

"""
全局配置
"""


def get_dataset_path(roots):
    exist_root = None
    for root in roots:
        if os.path.exists(root):
            exist_root = root
    if not exist_root:
        raise Exception('路径{}都不存在'.format(" ".join(roots)))
    return exist_root


SHOW_LOGS = True  # 是否显示日志

# 本地数据集路径
DATASET_ROOT = get_dataset_path([
    'D:\\dl_data\\sample',
    'F:\\data_deeplearning\\sample_data'
])

# 本地词嵌入路径
EMBEDDING_ROOT = get_dataset_path([
    'D:\\dl_data\\word_embedding',
    'F:\\data_deeplearning\\word_embedding'
])
