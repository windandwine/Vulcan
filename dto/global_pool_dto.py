# -*- coding:utf-8 -*-


class GlobalPoolDto:
    """
    全局池，存放全局变量
    """
    def __init__(self):
        self.__config = None  # 配置
        self.__embedding = None   # 词嵌入
        self.__dataset_op = None  # 数据dataset

    @property
    def config(self):
        return self.__config

    @config.setter
    def config(self, config):
        self.__config = config

    @property
    def embedding(self):
        return self.__embedding

    @embedding.setter
    def embedding(self, embedding):
        self.__embedding = embedding

    @property
    def dataset_op(self):
        return self.__dataset_op

    @dataset_op.setter
    def dataset_op(self, dataset_op):
        self.__dataset_op = dataset_op
