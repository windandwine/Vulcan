# -*- coding:utf-8 -*-


class DatasetHandleDto:
    """
    train, validation, test数据集自定义处理方法封装
    """
    def __init__(self, train_func, validation_func, test_func):
        self.__train_func = train_func
        self.__validation_func = validation_func
        self.__test_func = test_func

    @property
    def train_func(self):
        return self.__train_func

    @property
    def validation_func(self):
        return self.__validation_func

    @property
    def test_func(self):
        return self.__test_func
