# -*- coding:utf-8 -*-


class DividedDataSetDto:
    """
    初始数据集构建解析器需要的参数封装
    """
    def __init__(
            self, train_features, train_labels, train_num,
            validation_features=None, validation_labels=None, validation_num=None,
            test_features=None, test_labels=None, test_num=None):
        """
        test可以不传, 只划分训练集和验证集
        """
        # 特征
        self.__train_features = train_features
        self.__validation_features = validation_features
        self.__test_features = test_features
        # labels
        self.__train_labels = train_labels
        self.__validation_labels = validation_labels
        self.__test_labels = test_labels
        # 样本数量
        self.__train_num = train_num
        self.__validation_num = validation_num
        self.__test_num = test_num

    @property
    def train_features(self):
        return self.__train_features

    @train_features.setter
    def train_features(self, train_features):
        self.__train_features = train_features

    @property
    def validation_features(self):
        return self.__validation_features

    @validation_features.setter
    def validation_features(self, validation_features):
        self.__validation_features = validation_features

    @property
    def test_features(self):
        return self.__test_features

    @test_features.setter
    def test_features(self, test_features):
        self.__test_features = test_features

    @property
    def train_labels(self):
        return self.__train_labels

    @train_labels.setter
    def train_labels(self, train_labels):
        self.__train_labels = train_labels

    @property
    def validation_labels(self):
        return self.__validation_labels

    @validation_labels.setter
    def validation_labels(self, validation_labels):
        self.__validation_labels = validation_labels

    @property
    def test_labels(self):
        return self.__test_labels

    @test_labels.setter
    def test_labels(self, test_labels):
        self.__test_labels = test_labels

    @property
    def train_num(self):
        return self.__train_num

    @train_num.setter
    def train_num(self, train_num):
        self.__train_num = train_num

    @property
    def validation_num(self):
        return self.__validation_num

    @validation_num.setter
    def validation_num(self, validation_num):
        self.__validation_num = validation_num

    @property
    def test_num(self):
        return self.__test_num

    @test_num.setter
    def test_num(self, test_num):
        self.__test_num = test_num
