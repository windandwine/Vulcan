# 目录

- 一、项目简介
  - 1.项目简介
  - 2.项目目标
- 二、项目结构
  - 1.库
  - 2.文件结构
  - 2.项目结构图
- 三、全局配置
  - 1.数据集本地文件夹
- 四、简明样例
  - 1.定义net结构
  - 2.定义配置文件
    - 2.1train配置
    - 2.2predict配置
  - 1.训练
  - 2.预测
- 五、打印和tensorboard
  - 1.配置
  - 2.网络结构
  - 3.训练过程
  - 4.tensorboard

# 一、项目简介

## 1.项目简介

Vulcan（匠神）项目是基于**tensorflow**的深度学习复用框架，旨在封装深度模型搭建过程中的复用部分（pre-processing、evaluate和validate等），提高搭建模型的效率，更聚焦与模型本身。

Vulcan**使用配置（.yml）控制训练**、预测流程，封装训练、预测参数，复用只需更改配置，简单快捷。

## 2.项目目标

- [x] 1.提取深度学习工程的核心流程，复用化封装（已完成简单核心）。
- [x] 2.一行代码训练，一行代码预测。
- [ ] 3.封装DNN、CNN和RNN的常规模型。
- [ ] 4.可视化搭建网络。
- [ ] 5.一键导出。

# 二、项目结构

## 1.库

需要安装到的库

```
tqdm
pyyaml
numpy
pandas
tensorflow
```

## 2.文件目录

- base
  - main 入口方法
- config 
  - core **核心配置，存放控制训练预测的.yml文件**
    - default 默认配置
    - train 训练配置
    - predict 预测配置
  - glob
    - config 一些全局配置
    - global_pool 全局变量池，供整个运行时的全局加载
  - load_config.py 入口
- data 训练数据
  - board tensorboard文件
  - log 日志文件
  - model 训练出的模型文件
  - model_img 模型结构图
- dataset 
  - parser 解析器封装（主要使用tf.data.Dataset）
  - preprocessor 预处理器
  - reader 读取器，读取原始数据集
  - build_dataset.py 入口
- dto 数据传输对象
- eval 校验方法
- element 复用方法（CNN、RNN复用方法，如conv2d、pool等的进一步封装）
- embed 词嵌入模块
- eval 验证方法
- model 模型核心网络
  - base 模型核心（无需关注）
  - loss 损失函数模块
  - net 网络结构
  - optimizer 优化器
  - predict 自定义预测方法
  - load_model.py 入口
- test 测试方法
- utils 工具包
- train.py 一行代码训练样例
- predict.py 一行代码预测样例

## 3.项目结构图

![输入图片说明](https://images.gitee.com/uploads/images/2019/1017/154900_a8afaf12_1295352.png "readme.png")

# 三、多设备配置

在**多个设备**上运行本项目，多个设备上的数据集路径可能不同时，请配置 config/glob/config.py 中的DATASET_ROOT，运行项目时**在train配置文件中的reader.dataset使用最后一个文件夹名**即可。

如果不需多设备频繁切换，则可以忽略配置本项，**在train配置文件中的reader.dataset使用绝对路径**即可。

```python
# 本地数据集路径
DATASET_ROOT = get_dataset_path([
    'D:\\dl_data\\sample',
    'F:\\data_deeplearning\\sample_data'
])
```

# 四、简明样例

构建一个单层dnn作为样例

## 1.定义net结构

一般放在定义的net结构放在 model/net 路径下，定义的结构如下：

```python
def net(self):
    with tf.name_scope('fc_1'):
        layer1 = fc_layer(self.xs, 10, activation_func=tf.nn.softmax)  # 隐藏层
    with tf.name_scope('fc_2'):
        self.y_pred = fc_layer(layer1, 10, activation_func=tf.nn.softmax)  # 隐藏层
```

**注意：形参必须写self，输入写self.xs即可，最终的输出一定要赋给self.y_pred**，输入的shape和dtype暂不关注，后续会在配置文件中配置。

## 2.定义配置文件

配置文件的含义和限制还在项目中查看 config/core/train/templet.yml和readme文档。

### 2.1train配置

```yaml
#--- dataset.reader模块配置----
reader:
  dataset: 'mnist'
  module: 'dataset.reader.mnist_reader'
#--- model.net配置-----
net:
  module: 'model.net.dnn.single_layer.single_layer'
#--- model初始化配置-----
xs_shape: [784]
xs_dtype: 'float'
ys_shape: [10]
ys_dtype: 'float'
#--- model train配置-----
epoch: 2
batch_size: 60
# 保存配置
save:
  is_save: false
# 损失函数
optimizer:
  name: sgd
```

### 2.2predict配置

```yaml
#--- common配置-----
module: 'model.net.dnn.single_layer.single_layer'
xs_shape: [784]  # xs shape
#--- predict配置-----
load_model_dir: "data/model/dnn/single_layer/10151326/model"
predict_mod: 'model.predict.default_predict'
```

## 3.训练

将train的配置文件传给train()，一键即可训练。

```python
from base.main import train
if __name__ == '__main__':
    train('config/core/train/single_layer.yml')
```

## 4.预测

将predict的配置文件传给predict()，一键即可训练。

```python
from base.main import predict
if __name__ == '__main__':
    # 一行预测
    y_pred = predict('config/core/predict/single_layer.yml', x)
    # 打印结果
    print('\033[35my:{}, y_pred:{}'.format(np.argmax(y, axis=1), np.argmax(y_pred, axis=1)))
```

# 五、打印和tensorboard

项目日志如下

## 1.配置
![输入图片说明](https://images.gitee.com/uploads/images/2019/1017/232613_9c416197_1295352.png "config_log.png")

## 2.网络结构

![输入图片说明](https://images.gitee.com/uploads/images/2019/1017/232620_5314f1cc_1295352.png "net.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/1017/232629_7fc9aa8f_1295352.png "net2.png")

## 3.训练过程

![输入图片说明](https://images.gitee.com/uploads/images/2019/1017/232637_cfd30a89_1295352.png "train.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/1017/232645_275fc5d4_1295352.png "train2.png")

## 4.tensorboard

会自动在浏览器弹出tensorboard，截图如下

![输入图片说明](https://images.gitee.com/uploads/images/2019/1017/232652_2cb1cad4_1295352.png "tensorboard.png")


项目融合了**embedding（词嵌入）、pre_process（预处理）、dataset（tf.data.Dataset）等模块**。

其中**各个模块均抽取出可自定义的部分，可以通过配置直接选择已定义好的，或者自行写代码定义，在配置中配置即可**。详细文档请移步本项目Wiki。



