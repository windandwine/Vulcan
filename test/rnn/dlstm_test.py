# -*- coding:utf-8 -*-

from base.main import train, predict
from utils.io_utils import convert_abspath

"""
Deep LSTM 模型测试
"""

dlstm_train_config = convert_abspath('config/core/train/dlstm.yml')
dlstm_predict_config = convert_abspath('config/core/predict/dlstm.yml')


def dlstm_train():
    """
    Deep LSTM 训练
    :return:
    """
    train(dlstm_train_config)


def dlstm_predict():
    """
    Deep LSTM 预测
    :return:

    """
    # 抽取两条
    x = '秋'
    # 预测
    y_pred = predict(dlstm_predict_config, x)
    print(y_pred)


if __name__ == '__main__':
    # dlstm_train()
    dlstm_predict()
