# -*- coding:utf-8 -*-

import importlib
from config.glob.global_pool import global_pool
import config.glob.config as global_cfg
from utils.log_utils import log_path


"""
每种数据集的格式、类型不同，加载文件的方式需要自定义，定义在此文件夹
"""


def split_dataset_dir():
    """
    拼接数据集文件夹路径
    :return:
    """
    # 判断是否已经绝对路径, 以适配直接在配置中写明直接路径
    predix = global_cfg.DATASET_ROOT + '\\' if ':' not in global_pool.config.reader.dataset else ''
    ds_dir = '{}{}\\'.format(predix, global_pool.config.reader.dataset)  # 数据集绝对路径
    log_path(ds_dir, 'dataset path')
    return ds_dir


def read_data():
    """
    自定义打开数据集方法，加载原始数据
    :return:
    """
    if global_pool.config.reader:
        module = global_pool.config.reader.module  # 配置中打开数据集模块
        load_func = importlib.import_module(module)
        ds_dir = split_dataset_dir()
        ori_data = load_func.read(ds_dir)  # 必须返回DividedDataSet对象
    else:
        raise Exception('Need \'reader\' in config')
    return ori_data
