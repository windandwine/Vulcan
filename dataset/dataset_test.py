# -*- coding:utf-8 -*-

from dataset.parser.parser import parse
from config.glob.global_pool import global_pool

"""
测试解析器
"""


def run():
    parser = parse(global_pool.config)
    return parser


if __name__ == '__main__':
    dataset = run()
    dataset.train.init_itreator()
    for i in range(2):
        x, y = dataset.train.next_batch()
        print(i, x, y)


