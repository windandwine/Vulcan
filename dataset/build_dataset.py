# -*- coding:utf-8 -*-

import importlib
from config.glob.global_pool import global_pool
from utils.log_utils import ClampLog
from dto.dataset_handle_dto import DatasetHandleDto
from dataset.reader.reader import read_data
from dataset.preprocessor.preprocesser import preprocess
from dataset.parser.parser import parse

"""
动态加载Dataset解析器
"""


def build_dataset():
    """
    构建dataset
    :return:
    """
    # 读取数据集
    with ClampLog('reading dataset'):
        data = read_data()
    # 预处理
    with ClampLog('preprocessing'):
        data = preprocess(data)
    # 构建tf.data.Dataset
    with ClampLog('building tf.data.Dataset'):
        parse(data)
