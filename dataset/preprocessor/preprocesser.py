# -*- coding:utf-8 -*-

import importlib
from utils.log_utils import log_debug
from config.glob.global_pool import global_pool

"""
预处理，如果有预处理，分割数据集放在预处理里
"""


def preprocess(data):
    """
    训练预处理
    :param data:
    :return:
    """
    if global_pool.config.preprocess.use:  # 是否使用预处理
        # 加载预处理方法
        pre_module = global_pool.config.preprocess.module  # 加入前缀
        preprocess_module = importlib.import_module(pre_module)
        if global_pool.config.is_train:  # 训练部分预处理
            data_pre = preprocess_module.train_pre(data)
        else:  # 预测部分预处理
            data_pre = preprocess_module.predict_pre(data)
        return data_pre
    else:
        log_debug('no preprocess config, do not load preprocessor', fore='dy')
        return data


