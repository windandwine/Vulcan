# -*- coding:utf-8 -*-

import os
import time
import yaml
from utils import root_dir
from utils.log_utils import log_path

"""
IO工具
"""


def convert_abspath(related_path):
    """
    加上根路径，得到绝对路径
    :param related_path:
    :return:
    """
    return root_dir + '/' + related_path


def generate_model_dir(config):
    """
    根据net.module和时间生成保存model与tensorboard的path
    :return:
    """
    model_name = '_'.join(config.net.module.split('.')[-3:-1])
    folder_dir = config.save.model_dir if config.save.model_dir else 'data/model/{}/'.format(model_name)
    folder_dir = convert_abspath(folder_dir + '{}/'.format(time.strftime("%y%m%d%H%M", time.localtime())))
    if not os.path.exists(folder_dir):  # 不存在文件夹则新建
        os.makedirs(folder_dir)
    return folder_dir


def get_yml(yml_path):
    """
    获得yml文件
    :param yml_path:
    :return:
    """
    config = yaml.safe_load(open(yml_path, encoding='utf-8'))
    return config


def module_to_path(module_str):
    """
    module类型字符串转path类型
    :param module_str:
    :return:
    """
    path = '/'.join(module_str.split('.'))
    return path


def path_to_module(path_str):
    """
    path类型字符串转module类型
    :param path_str:
    :return:
    """
    path = '.'.join(path_str.split('/'))
    return path
