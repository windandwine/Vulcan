# -*- coding:utf-8 -*-

from tensorflow.core.framework import summary_pb2
import config.glob.config as glob_cfg
from config.glob.params import PRINT_BACK, PRINT_FORE, PRINT_STYLE
import logging


"""
日志工具
"""

logging.basicConfig(level=logging.DEBUG, format='%(message)s')


def color_str(msg, fore='def', back='def', style='def'):
    """
    字符串上色
    :param msg:
    :param fore:
    :param back:
    :param style:
    :return:
    """
    fore = 'lw' if fore == 'def' and back != 'def' else fore
    prefix = '\033['
    prefix = prefix + PRINT_STYLE.get(style, '0') + ';'  # 显示方式
    prefix = prefix + PRINT_FORE.get(fore, '37') + ';'  # 前景色
    prefix = prefix + PRINT_BACK.get(back, '') + 'm'  # 背景色
    string = prefix + msg + '\033[0m'
    return string


class ClampLog:
    """
    切片日志，with模块包裹
    """
    def __init__(self, msg):
        self.msg = msg

    def __enter__(self):
        if glob_cfg:
            logging.debug(color_str('▼▼Begin ' + self.msg, fore='lw'))

    def __exit__(self, type, value, trace):
        if glob_cfg:
            logging.debug(color_str('▲▲Finish ' + self.msg, fore='lw'))


def log_debug(msg, fore='def', back='def', style='def'):
    """
    Debug日志封装在此，方便控制
    :param msg:
    :param fore: 前景色
    :param back: 背景色
    :param style: 显示方式
    :return:
    """
    if not (isinstance(msg, tuple) or isinstance(msg, list)):
        c_str = color_str(msg, fore=fore, back=back, style=style)
        logging.debug(c_str)
    else:  # 传入msg为list或tuple
        val_fore = fore == 'def' or isinstance(fore, tuple) or isinstance(fore, list)
        val_back = back == 'def' or isinstance(back, tuple) or isinstance(back, list)
        val_style = style == 'def' or isinstance(style, tuple) or isinstance(style, list)
        if not val_fore or not val_back or not val_style:
            raise ValueError('msg为tuple或list, 则fore, back, style必须为tuple或list或者不传参')
        c_str = ''
        for i in range(len(msg)):
            c_str = c_str + color_str(
                msg[i],
                fore='def' if fore == 'def' else fore[i],
                back='def' if back == 'def' else back[i],
                style='def' if style == 'def' else style[i]
            ) + ' '
        logging.debug(c_str)


def log_path(path, desc=''):
    """
    路径打印
    :param path:
    :param desc:
    :return:
    """
    log_debug([desc, path], fore=['dy', 'b'], style=['def', 'un'])


def make_summary(name, val):
    """
    记录不在graph中的数据
    :param name:
    :param val:
    :return:
    """
    value = summary_pb2.Summary.Value(tag=name, simple_value=val)
    return summary_pb2.Summary(value=[value])


if __name__ == '__main__':
    a = {'a': 1}
    c = a.get(None, 'a')
    print(c)
