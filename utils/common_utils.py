# -*- coding:utf-8 -*-

import inspect

"""
常规工具
"""


class Singleton(object):
    """
    单例父类, 继承此类的子类为单例
    """
    _inst = None

    def __new__(cls, *args,  **kwargs):
        if not cls._inst:
            cls._inst = super(Singleton, cls).__new__(cls)
        return cls._inst


def get_func_return_name(func):
    """
    获得函数的返回值名称(自省)
    :param func:
    :return:
    """
    if inspect.isfunction(func):
        return_line = inspect.getsourcelines(func)[0][-1]  # 获取最后一行代码

        if return_line.strip().startswith('return'):  # return开头
            return_line = return_line.replace('return ', '')
            return_name = return_line.split(',')
            names = [name.strip() for name in return_name]
        else:
            raise ValueError('函数返回值解析，函数无返回值')
    else:
        raise ValueError('函数返回值解析，传入参数必须为函数')
    return names


def get_func_param(func, param):
    """
    获取函数参数
    :param func:
    :param param:
    :return:
    """
    return inspect.signature(func).parameters.get(param)
