# -*- coding:utf-8 -*-

import tensorflow as tf


"""
计算工具
"""


def multi_add(*args):
    """
    多参数相加（需要shape相同）
    :param args:
    :return:
    """
    num = len(args)
    if num < 2:
        raise ValueError("至少两数才能相加")
    params = args
    while num > 1:
        temp = []
        for idx in range(num // 2):
            temp.append(tf.add(params[idx], params[num-idx-1]))
        if num % 2 == 1:
            temp.append(params[num // 2])
        num = len(temp)
        params = temp
    return params[0]
