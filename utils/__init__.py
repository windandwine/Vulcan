# -*- coding:utf-8 -*-
import os

__all__ = ['root_dir', 'dataset_utils', 'io_utils', 'log_utils']

# 获取根目录
root_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__))).replace('\\', '/')

