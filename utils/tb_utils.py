# -*- coding:utf-8 -*-

import webbrowser
import threading
import subprocess
from utils.log_utils import log_debug, log_path

"""
tensorboard 工具
"""


def cmd_run_tensorboard(log_dir, tb_port):
    """
    cmd打开tensorboard
    :param log_dir: 日志路径
    :param tb_port: 端口
    :return:
    """
    command = 'tensorboard --logdir={} --host={} --port={}'.format(log_dir, 'localhost', tb_port)
    process = subprocess.Popen(
        command, shell=True,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT
    )
    while True:
        if not process.poll():  # 子进程运行中
            line = process.stdout.readline()
            line_str = str(line, encoding="utf-8")
            if line_str.startswith('TensorBoard'):
                url = line_str[line_str.find('http'): line_str.find('(')].strip()
                if url:
                    # 打开浏览器跳转url
                    log_path(url, 'tensorboard url')
                    webbrowser.open(url)
                    # 结束进程
                    # process.kill()
                else:
                    raise Exception('tensorboar start error, please run again')


class TBThread (threading.Thread):
    """
    自定义线程
    """
    def __init__(self, thread_id, name, log_dir, tb_port):
        threading.Thread.__init__(self)
        self.threadID = thread_id
        self.name = name
        self.log_dir = log_dir
        self.tb_port = tb_port

    def run(self):
        log_path(self.log_dir, "tensorboarad is opening soon")
        cmd_run_tensorboard(self.log_dir, self.tb_port)


def kill_tensorboard(tb_port):
    """
    清除所有tensorboard进程(如果端口>6006,则不用)
    :param tb_port:
    :return:
    """
    if tb_port == 6006:
        process = subprocess.Popen(
            "taskkill /F /IM tensorboard.exe", shell=True,
            stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        cmd_log = process.stdout.read()
        cmd_log_str = str(cmd_log)
        process.wait()
        log_debug('close all tensorbord.exe, {}'.format(cmd_log_str), fore='dy')


def run_tensorboard(log_dir, tb_port):
    """
    运行tensorboard 整体流程
    :param log_dir: 日志路径
    :return:
    """
    # kill已经打开的所有tensorboard
    kill_tensorboard(tb_port)
    # 创建新线程
    thread1 = TBThread(1, "TensorboardThread", log_dir, tb_port)
    # 开启新线程
    thread1.start()
    # thread1.join()


if __name__ == '__main__':
    run_tensorboard('data/board/rnn/dlstm/10180843/')
    stra = 'TensorBoard 1.13.1 at http://LAPTOP-CAO2E5OD:6006 (Press CTRL+C to quit)'
    sub = stra[stra.find('http'): stra.find('(')].strip()
    print(sub)
